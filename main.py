#!/usr/bin/env python3

#+++
#Author: Holychicken99
#+++
import re
from jinja2 import Environment, FileSystemLoader
import os

from datetime import datetime


events_list = []

class Event:
    def __init__(self, name, description, day, date, location, time, meridiemIndicator):
        self.name = name
        self.description = description
        self.day = day
        self.date =datetime.strptime(date, "%d-%m-%Y")
        self.location = location
        self.time = time
        self.meridiemIndicator = meridiemIndicator

def read_events_file(file_path):
    current_event = {}

    file_content = open(file_path, 'r').read()


    event_pattern = re.compile(
        r"Event: (?P<name>.*?)\nDescription: (?P<description>.*?)\nday: (?P<day>.*?)\ndate: (?P<date>.*?)\nlocation: (?P<location>.*?)\ntime: (?P<time>.*?)\nmeridiemIndicator: (?P<meridiemIndicator>.*?)\n",
        re.DOTALL
    )
    match = re.finditer(event_pattern, file_content)
    for event in match:
        name = event.group('name')
        description = event.group('description')
        day = event.group('day')
        date = event.group('date')
        location = event.group('location')
        time = event.group('time')
        meridiemIndicator = event.group('meridiemIndicator')

        events_list.append(Event(name, description, day, date, location, time, meridiemIndicator))


env = Environment(loader=FileSystemLoader('./'))
read_events_file("./events/list.md")

template = env.get_template('./events/template.html')
output = template.render(events=events_list)

output_file = './events/index.html'

if os.path.exists(output_file):

    # Delete the file
    os.remove(output_file)

with open(output_file, 'w') as output_file:
    output_file.write(output)
